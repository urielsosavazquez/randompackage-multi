package me.randomhashtags.randompackage.enums;

public enum KOTHStatus {
    NOT_SETUP,
    NOT_ACTIVE,
    STOPPED,
    CAPTURED,
    ACTIVE,
}
