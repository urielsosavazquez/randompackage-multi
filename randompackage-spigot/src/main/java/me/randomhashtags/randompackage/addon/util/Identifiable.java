package me.randomhashtags.randompackage.addon.util;

public interface Identifiable {
    String getIdentifier();
}
